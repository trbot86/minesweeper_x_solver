import sys
import traceback
from time import sleep
from gmpy2 import popcount

from common import *
from game_interface import *

TAKE_GUESSES = True
#TAKE_GUESSES = False

def get_adj(board, w, h, cellx, celly):
    return [(x, y) for y in range(max(0, celly-1), min(h, celly+2))
                   for x in range(max(0, cellx-1), min(w, cellx+2))
                   if x != cellx or y != celly]

def validate(board, w, h):
    for (x, y) in [(x, y) for x in range(w) for y in range(h)]:
        v = board[y][x]
        if not (v >= 1 and v <= 8): continue

        adj = get_adj(board, w, h, x, y)
        unclicked_adj = [(x2, y2) for (x2, y2) in adj if board[y2][x2] == UNCLICKED_CELL]
        known_mines_adj = [(x2, y2) for (x2, y2) in adj if board[y2][x2] == FLAG_CELL]
        
        # cell has too many adjacent mines
        if len(known_mines_adj) > v: return False
        
        # cell has too few possible adjacent mines
        if len(known_mines_adj) + len(unclicked_adj) < v: return False
        
    return True

def take_easy_steps(gi, board, w, h, only_simulate = False):
    #if not only_simulate: print 'invoking take_easy_steps'
    # look for moves to make
    flagged = set()    # only flag each cell once, to avoid flagging + unflagging in a loop forever
    revealed = set()   # similar for revealing cells
    for (x, y) in [(x, y) for x in range(w) for y in range(h)]:
        v = board[y][x]
        if v == UNCLICKED_CELL or v == EMPTY_CELL or v == FLAG_CELL: continue

        adj = get_adj(board, w, h, x, y)
        unclicked_adj = [(x2, y2) for (x2, y2) in adj if board[y2][x2] == UNCLICKED_CELL]
        known_mines_adj = [(x2, y2) for (x2, y2) in adj if board[y2][x2] == FLAG_CELL]

        # flag adjacent cells that ARE mines
        if v == len(unclicked_adj) + len(known_mines_adj):
            for (x2, y2) in unclicked_adj:
                if (x2, y2) not in flagged:
                    flagged.add((x2, y2))
                    board[y2][x2] = FLAG_CELL
                    if not only_simulate: gi.click_cell(x2, y2, True) # right click

        # reveal adjacent cells that are NOT mines
        if not only_simulate:
            if v == len(known_mines_adj):
                for (x2, y2) in unclicked_adj:
                    if (x2, y2) not in revealed:
                        revealed.add((x2, y2))
                        board[y2][x2] = REVEALING_CELL # we don't know the true value until the next screenshot, so, for now, use this special value
                        if not only_simulate: gi.click_cell(x2, y2) # left click
    
    if VERBOSE and not only_simulate and (flagged != set() or revealed != set()):
        print 'discovered with easy steps: flagged=' + repr(flagged) + ' revealed=' + repr(revealed)
    return flagged, revealed

def count_bits(i):
    return popcount(i)

def get_bit(i, bit):
    return (i >> bit) & 1

def is_adjacent(cell1, cell2):
    diffx = abs(cell1[X] - cell2[X])
    diffy = abs(cell1[Y] - cell2[Y])
    return diffx <= 1 and diffy <= 1

def is_adjacent_to_all(cell, all):
    for c in all:
        if not is_adjacent(cell, c): return False
    return True

def get_numbered_cells_that_are_adj_to_everything_in_allset_but_notin_allset(board, w, h, allset):
    result = set()
    for cell in allset:
        adj = get_adj(board, w, h, cell[X], cell[Y])
        for cell2 in adj:
            if cell2 in result or cell2 in allset: continue
            v2 = board[cell2[Y]][cell2[X]]
            if not (v2 >= 1 and v2 <= 8): continue
            if is_adjacent_to_all(cell2, allset): result.add(cell2)
    return result

def take_hard_steps(gi, board, w, h, only_simulate = False):
    #print 'invoking take_hard_steps'
    flagged = set()
    revealed = set()
    
    # ALGORITHM:
    #   for each numbered cell
    #       compute remaining-mines
    #       skip if 0
    #       get adjacent-unclicked
    #       candidates = get all numbered cells that are adjacent to ALL of adjacent-unclicked
    #       for each cell2 in candidates
    #           compute remaining-mines2
    #           skip if 0
    #           candidate_locations = get adjacent-unclicked2 setminus adjacent-unclicked
    #           if remaining-mines2 - remaining-mines = len(candidate_locations)
    #               all are mines
    #           if remaining-mines2 - remaining-mines = 0
    #               all are safe
    
    for (x, y) in [(x, y) for x in range(w) for y in range(h)]:
        v = board[y][x]
        if not (v >= 1 and v <= 8): continue

        #print 'x=' + repr(x) + ' y=' + repr(y) + ' v=' + repr(v)
        adj = get_adj(board, w, h, x, y)
        #print 'adj=' + repr(adj)
        known_mines_adj = [(x2, y2) for (x2, y2) in adj if board[y2][x2] == FLAG_CELL]
        #print 'known_mines_adj=' + repr(known_mines_adj)
        remaining_mines = v - len(known_mines_adj)
        if remaining_mines == 0: continue
        #print 'remaining_mines=' + repr(remaining_mines)
        unclicked_adj = {(x2, y2) for (x2, y2) in adj if board[y2][x2] == UNCLICKED_CELL}
        #print 'unclicked_adj=' + repr(unclicked_adj)
        
        candidates = get_numbered_cells_that_are_adj_to_everything_in_allset_but_notin_allset(board, w, h, unclicked_adj)
        candidates = candidates - set([(x, y)]) # current cell is not a candidate :)
        #print 'candidates=' + repr(candidates)
    
        for (x2, y2) in candidates:
            v2 = board[y2][x2]
            adj2 = get_adj(board, w, h, x2, y2)
            known_mines_adj2 = [(x3, y3) for (x3, y3) in adj2 if board[y3][x3] == FLAG_CELL]
            remaining_mines2 = v2 - len(known_mines_adj2)
            if remaining_mines2 == 0: continue
            unclicked_adj2 = {(x3, y3) for (x3, y3) in adj2 if board[y3][x3] == UNCLICKED_CELL}
            
            candidate_locations = unclicked_adj2 - unclicked_adj
            if remaining_mines2 - remaining_mines == len(candidate_locations):
                # all candidate locations are mines
                for (x3, y3) in candidate_locations:
                    if (x3, y3) not in flagged:
                        flagged.add((x3, y3))
                        board[y3][x3] = FLAG_CELL
                        if not only_simulate: gi.click_cell(x3, y3, True)
            elif remaining_mines2 - remaining_mines == 0:
                # all candidate locations are safe
                for (x3, y3) in candidate_locations:
                    if (x3, y3) not in revealed:
                        revealed.add((x3, y3))
                        board[y3][x3] = REVEALING_CELL
                        if not only_simulate: gi.click_cell(x3, y3)
    
    if VERBOSE and not only_simulate and (flagged != set() or revealed != set()):
        print 'discovered with hard steps: flagged=' + repr(flagged) + ' revealed=' + repr(revealed)
    return flagged, revealed

def take_simulated_steps(gi, board, w, h):
    #print 'invoking take_simulated_steps'
    flagged = set()
    revealed = set()
    for (x, y) in [(x, y) for x in range(w) for y in range(h)]:
        v = board[y][x]
        if not (v >= 1 and v <= 8): continue

        adj = get_adj(board, w, h, x, y)
        unclicked_adj = [(x2, y2) for (x2, y2) in adj if board[y2][x2] == UNCLICKED_CELL]
        known_mines_adj = [(x2, y2) for (x2, y2) in adj if board[y2][x2] == FLAG_CELL]

        remaining_mines = v - len(known_mines_adj)
        if remaining_mines == 0: continue
        
        valid_placements = []
        for i in range(2**len(unclicked_adj)):
            if count_bits(i) == remaining_mines:
                # simulate mines being placed at elements of unclicked_adj
                # according to the 1-bits in i
                indices = [bit_ix for bit_ix in range(len(unclicked_adj)) if get_bit(i, bit_ix)]
                
                total_flagged = set()
                total_revealed = set()
                
                # flag all indices in unclicked_adj and run a simulation,
                # fleshing out all consequences of doing so
                # (given the currently available board information)
                for j in indices:
                    (x2, y2) = unclicked_adj[j]
                    board[y2][x2] = FLAG_CELL
                    total_flagged.add((x2, y2))
                
                while True:
                    _flagged, _revealed = take_easy_steps(gi, board, w, h, True)
                    total_flagged.update(_flagged)
                    total_revealed.update(_revealed)
                    if _flagged == set() and _revealed == set():
                        _flagged, _revealed = take_hard_steps(gi, board, w, h, True)
                        total_flagged.update(_flagged)
                        total_revealed.update(_revealed)
                        if _flagged == set() and _revealed == set(): break
                
                # try to validate the new board state
                if validate(board, w, h):
                    valid_placements.append((i, total_flagged, total_revealed))
                
                # undo all changes made by the simulation
                for (x2, y2) in total_flagged: board[y2][x2] = UNCLICKED_CELL
                for (x2, y2) in total_revealed: board[y2][x2] = UNCLICKED_CELL
        
        assert len(valid_placements) >= 1
        flagged_in_all = set.intersection(*[_flagged for (_placement, _flagged, _revealed) in valid_placements])
        revealed_in_all = set.intersection(*[_revealed for (_placement, _flagged, _revealed) in valid_placements])
        
        for (x2, y2) in flagged_in_all:
            if (x2, y2) not in flagged:
                flagged.add((x2, y2))
                board[y2][x2] = FLAG_CELL
                gi.click_cell(x2, y2, True)            
        for (x2, y2) in revealed_in_all:
            if (x2, y2) not in revealed:
                revealed.add((x2, y2))
                board[y2][x2] = REVEALING_CELL # we don't know the true value until the next screenshot, so, for now, use this special value
                gi.click_cell(x2, y2)
        
        # if there is only a single valid mine placement policy, then we know even more!
        if len(valid_placements) == 1:
            placement, _unused1, _unused2 = valid_placements[0]
            for bit_ix in range(len(unclicked_adj)):
                (x2, y2) = unclicked_adj[bit_ix]
                if get_bit(placement, bit_ix):
                    if (x2, y2) not in flagged:
                        flagged.add((x2, y2))
                        board[y2][x2] = FLAG_CELL
                        gi.click_cell(x2, y2, True)
                else:
                    if (x2, y2) not in revealed:
                        revealed.add((x2, y2))
                        board[y2][x2] = REVEALING_CELL # we don't know the true value until the next screenshot, so, for now, use this special value
                        gi.click_cell(x2, y2)
    
    if flagged != set() or revealed != set(): print 'discovered with simulated steps: flagged=' + repr(flagged) + ' revealed=' + repr(revealed)
    return flagged, revealed

def print_probabilities(prob, w, h, file_handle = sys.stdout):
    file_handle.write('Probabilities:\n')
    for y in range(h):
        for x in range(w):
            file_handle.write('%5.2f ' % prob[y][x])
        file_handle.write('\n')
    file_handle.write('\n')

def compute_probabilities(gi, board, w, h):
    if VERBOSE: print 'Must guess... computing probabilities...'
    prob = [[PROBABILITY_NOT_SET for x in range(w)] for y in range(h)]
    
    # union of disjoint regions where one or more mines are known to exist,
    # but whose precise locations are not known
    mine_regions = set()
    # number of mines known to be in these regions
    num_mines_in_mine_regions = 0
    
    # compute probabilities induced by numbered cells
    for (x, y) in [(x, y) for y in range(h) for x in range(w)]:
        v = board[y][x]
        if not (v >= 1 and v <= 8): continue
        
        adj = get_adj(board, w, h, x, y)
        unclicked_adj = [(x2, y2) for (x2, y2) in adj if board[y2][x2] == UNCLICKED_CELL]
        known_mines_adj = [(x2, y2) for (x2, y2) in adj if board[y2][x2] == FLAG_CELL]

        remaining_mines = v - len(known_mines_adj)
        if remaining_mines == 0: continue
        
        for (x2, y2) in unclicked_adj:
            p = float(remaining_mines) / float(len(unclicked_adj))
            prob[y2][x2] = max(prob[y2][x2], p)
        
        # add this region only if it is disjoint from mine_regions
        mine_region = set(unclicked_adj)
        if set.intersection(mine_regions, mine_region) == set():
            mine_regions.update(mine_region)
            num_mines_in_mine_regions = num_mines_in_mine_regions + remaining_mines

    # compute probabilities for cells that don't have probabilities induced
    #       by neighbouring numbered cells. we use the total number of
    #       mines remaining, and the number of mines known to be in the
    #       mine regions recorded above

    # each numbered unsatisfied cell induces an AREA with 1+ mines
    # if two areas are non-intersecting, they contain distinct mines
    # find the largest set of non-intersecting areas, and that gives the distinct mine count reserved for those areas.
    # that might be hard, so we can get /any/ set of non-intersecting areas.
    # total#mines - distinct mine count = # mines in other unclicked cells (that have no probability estimate)

    # to improve this further, we should try a few possibilities for the
    # selection of mine regions, and take the best one
    # (i.e., the one that encapsulates the most mines)

    num_unclicked = len([(x, y) for y in range(h) for x in range(w)
                                if board[y][x] == UNCLICKED_CELL])
    for (x, y) in [(x, y) for y in range(h) for x in range(w)]:
        if not prob[y][x] == PROBABILITY_NOT_SET: continue
        if not board[y][x] == UNCLICKED_CELL: continue
        if (x, y) in mine_regions: continue
        
        prob[y][x] = ((float(gi.mine_count) - float(num_mines_in_mine_regions))
                    / (float(num_unclicked) - float(len(mine_regions))))

    if VERBOSE: print_probabilities(prob, w, h)
    return prob

def take_guess_step(gi, board, w, h, prob):
    revealed = set()
    # get min nonzero prob
    minprob = 1
    minx = 0
    miny = 0
    for y in range(h):
        for x in range(w):
            if board[y][x] == UNCLICKED_CELL and prob[y][x] < minprob and prob[y][x] > 0:
                minx = x
                miny = y
                minprob = prob[y][x]
    
    if TAKE_GUESSES:
        if minprob < 1:
            if VERBOSE: print '**** GUESSING CELL x=' + repr(minx) + ', y=' + repr(miny) + ' with prob=' + repr(minprob) + ' ****'
            board[miny][minx] = REVEALING_CELL
            gi.click_cell(minx, miny)
            revealed.add((minx, miny))
        else:
    #        print 'MUST guess from unknown probabilities'
            print 'no guesses...'
    
    if VERBOSE and revealed != set(): print 'guess step: revealed=' + repr(revealed)
    return revealed
        
def print_board(board, w, h, file_handle = sys.stdout):
    file_handle.write('Printing board: w=' + repr(w) + ' h=' + repr(h) + '\n')
    for y in range(h):
        for x in range(w):
            if board[y][x] >= 0 and board[y][x] <= 8: file_handle.write(repr(board[y][x]) + ' ')
            else: file_handle.write(board[y][x] + ' ')
        file_handle.write('\n')
    file_handle.write('\n')

def do_main():
    gi = GameInterface('msx/msx.exe')
    active_last = False
    active = True
    last_guess_image_unclicked = 0
    while True:
        if DEBUG_TRACE: print '\n\n\nstart iteration'
        repr(gi._timer.reset())
        if DEBUG_TRACE: print 'start get_board' + gi._timer.split_str()
        result = gi.get_board()
        if DEBUG_TRACE: print 'end get_board' + gi._timer.split_str()
        if result == False:
            sleep(1)
            continue
        board, w, h = result

        if VERBOSE: print_board(board, w, h)

        # check if game is already over
        if DEBUG_TRACE: print 'check if game is already over' + gi._timer.split_str()
        active_last = active
        active = gi.state == IN_PROGRESS
        unclicked_cells = 0
        for (x, y) in [(x, y) for x in range(w) for y in range(h)]:
            if board[y][x] == UNCLICKED_CELL: unclicked_cells = unclicked_cells + 1
        
        if not active:
            if active_last:
                print 'Game ended:'
                if VERBOSE: print_board(board, w, h)
            if DEBUG_TRACE: print 'game ended' + gi._timer.split_str()
            continue
        
        if DEBUG_TRACE: print 'take easy steps' + gi._timer.split_str()
        flagged, revealed = take_easy_steps(gi, board, w, h)
        progress = not (flagged == set() and revealed == set())
        if DEBUG_TRACE: print 'done easy steps' + gi._timer.split_str()
        
        if not progress:
            # we didn't make progress with take_easy_steps
            if DEBUG_TRACE: print 'take hard steps' + gi._timer.split_str()
            flagged, revealed = take_hard_steps(gi, board, w, h)
            progress = not (flagged == set() and revealed == set())
            if DEBUG_TRACE: print 'done hard steps' + gi._timer.split_str()
        
        if not progress:
            # we didn't make progress with take_hard_steps
            if DEBUG_TRACE: print 'take simulated steps' + gi._timer.split_str()
            flagged, revealed = take_simulated_steps(gi, board, w, h)
            progress = not (flagged == set() and revealed == set())
            if DEBUG_TRACE: print 'done simulated steps' + gi._timer.split_str()
        
        if not progress:
            # still to add:
            #     - mine probability estimation and cell guessing.
            #       (visualize probabilities like a heatmap?)
            #       (learn overlay graphics in python?)
            if DEBUG_TRACE: print 'compute probabilities' + gi._timer.split_str()
            prob = compute_probabilities(gi, board, w, h)
            if DEBUG_TRACE: print 'done compute probabilities' + gi._timer.split_str()

            # record the fact that we're going to guess a cell
            if not unclicked_cells == last_guess_image_unclicked:
                fname_prefix = 'guess_images/' + repr(current_milli_time()) + '_' + repr(unclicked_cells) + 'unclicked'
                ftext = open(fname_prefix + '.txt', 'w')
                ftext.write('remaining unclicked=' + repr(unclicked_cells) + '\n')
                ftext.write('remaining mines=' + repr(gi.mine_count) + '\n')
                print_board(board, w, h, ftext)
                print_probabilities(prob, w, h, ftext)
                ftext.close()
                gi._img.save(fname_prefix + '.png', 'PNG')
                last_guess_image_unclicked = unclicked_cells

            revealed = take_guess_step(gi, board, w, h, prob)
            if DEBUG_TRACE: print 'take guess step' + gi._timer.split_str()
            progress = not (revealed == set())
            
        if progress:
            print 'flagged ' + repr(flagged)
            print 'revealed ' + repr(revealed)
            
        if progress and VERBOSE: print_board(board, w, h)
        else: sleep(1)
        if DEBUG_TRACE: print 'end iteration progress=' + repr(progress) + gi._timer.split_str()

if __name__ == "__main__":
    try:
        do_main()
    except:
        traceback.print_exc()
        handle_exception()
