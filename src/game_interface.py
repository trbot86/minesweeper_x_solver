__author__ = "trbot"
__date__ = "$Jul 8, 2017 8:36:32 PM$"

from common import *
from image_dict import *
#from time import sleep
from time import *
from pyrobot import Robot
from pywinauto import application
from PIL import Image
import numpy as np

UNCLICKED_CELL = "."
REVEALING_CELL = "?" # special value just used to represent a cell that will be updated in the next screenshot, but we don't yet know the true value
EMPTY_CELL = 0
MINE_CELL = "x"
MINE_CELL_CLICKED = MINE_CELL
NOT_A_CELL = "*"
FLAG_CELL = "!"

IMAGE_PATH = 'images/'

cell_imghashes = {}
mine_count_digit_imghashes = {}
unknown_imghashes = {}

imagedict = ImageDict(IMAGE_PATH)
imagedict_mapping = {
    "digit0.png":       (0, mine_count_digit_imghashes),
    "digit1.png":       (1, mine_count_digit_imghashes),
    "digit2.png":       (2, mine_count_digit_imghashes),
    "digit3.png":       (3, mine_count_digit_imghashes),
    "digit4.png":       (4, mine_count_digit_imghashes),
    "digit5.png":       (5, mine_count_digit_imghashes),
    "digit6.png":       (6, mine_count_digit_imghashes),
    "digit7.png":       (7, mine_count_digit_imghashes),
    "digit8.png":       (8, mine_count_digit_imghashes),
    "digit9.png":       (9, mine_count_digit_imghashes),
    "cell1.png":        (1, cell_imghashes),
    "cell2.png":        (2, cell_imghashes),
    "cell3.png":        (3, cell_imghashes),
    "cell4.png":        (4, cell_imghashes),
    "cell5.png":        (5, cell_imghashes),
    "cell6.png":        (6, cell_imghashes),
    "cell7.png":        (7, cell_imghashes),
    "cell8.png":        (8, cell_imghashes),
    "flagged.png":      (FLAG_CELL, cell_imghashes),
    "bottom.png":       (NOT_A_CELL, cell_imghashes),
    "right.png":        (NOT_A_CELL, cell_imghashes),
    "unclicked.png":    (UNCLICKED_CELL, cell_imghashes),
    "unclicked2.png":   (UNCLICKED_CELL, cell_imghashes),
    "empty.png":        (EMPTY_CELL, cell_imghashes),
    "empty2.png":       (EMPTY_CELL, cell_imghashes),
    "mine.png":         (MINE_CELL, cell_imghashes),
    "mineclicked.png":  (MINE_CELL, cell_imghashes),
}

current_milli_time = lambda: int(time() * 1000)

from datetime import datetime
from datetime import timedelta
start_time = datetime.now()
# returns the elapsed milliseconds since the start of the program
def millis():
   dt = datetime.now() - start_time
   ms = (dt.days * 24 * 60 * 60 + dt.seconds) * 1000 + dt.microseconds / 1000.0
   return ms

class milli_timer:
    def __init__(self):
        self._start = millis()
        self._prev = self._start
    
    def split_str(self):
        prev = self._prev
        self._prev = millis()
        return ' (' + repr((self._prev - prev)/1000.0) + 's, total ' + repr((self._prev - self._start)/1000.0) + 's)'
    
    def split(self):
        prev = self._prev
        self._prev = millis()
        print 'split=' + repr((self._prev - prev)/1000.0) + 's, total=' + repr((self._prev - self._start)/1000.0) + 's'
    
    def reset(self):
        self._start = millis()

class GameInterface:
    def __init__(self, path_to_minesweeper_x_exe):
        self._path = path_to_minesweeper_x_exe
        self._title = 'Minesweeper X'
        
        self._CELL_SIZE                         = 16
        self._CELL_CENTER_OFFSET                = (self._CELL_SIZE//2, self._CELL_SIZE//2)
        self._BOARD_OFFSET_FROM_TL_CORNER       = (12, 55)
        self._MINE_COUNT_OFFSET_FROM_TL_CORNER  = (17, 16)
        self._SMILEY_GAME_WON_IF_BLACK_OFFSET   = (0, 7)
        self._SMILEY_GAME_LOST_IF_BLACK_OFFSET  = (0, 3)
        self._MINE_COUNT_NUM_DIGITS             = 3
        self._MINE_COUNT_DIGIT_DIMS             = (13, 23)
        self._COLOR_START_BUTTON_SMILEY_BG      = (255, 255, 0  )
        self._COLOR_WHITE_TL_CORNER             = (255, 255, 255)

        self._app = application.Application()
        self._app.start(self._path)
#        self._app.connect(path = self._path)
        self._app_dialog = self._app.top_window_()
        self._app_dialog.SetFocus()
        self._app_dialog.Wait('ready', timeout=5, retry_interval=0.5)
        sleep(0.5)

        self._timer = milli_timer()
        self._robot = Robot(self._title)
        np.seterr(over='ignore')
        
        # computing imghashes from images
        for fname, img in imagedict.images.iteritems():
            px = img.load()
            w, h = img.size
            imagehash = self._compute_imagehash(px, (0, 0, w, h))
            if not imagedict_mapping.get(fname) == None:
                value, target_dict = imagedict_mapping[fname]
                target_dict[imagehash] = value
                print fname + ' image hash is ' + repr(imagehash) + ' and value=' + repr(value)
            else:
                print fname + ' is not a known image! if appropriate, add it to imagedict_mapping...'
        
    def get_board(self):
        if self._capture() == False: return False
        return (self._board, self._board_dims[W], self._board_dims[H])

    def click_cell(self, cellx, celly, right_click = False):
        # TODO: make click_cell first check whether the window has focus
        if VERBOSE: print ('Flagging' if right_click else 'Revealing') + ' cell ' + repr((cellx, celly))
        center_pos = self._get_cell_center(cellx, celly)
        self._robot.set_mouse_pos(center_pos[X], center_pos[Y])
        self._robot.click_mouse(button=('right' if right_click else 'left'))
        sleep(0.02)

    def _capture(self):
        self.state = UNKNOWN
        if DEBUG_TRACE: print "start capture" + self._timer.split_str()
        self._refresh_bounds()
        if DEBUG_TRACE: print "_refresh_bounds" + self._timer.split_str()
        #print 'game window rect: ' + repr(self._bounds) + ' w=' + repr(self._w) + ' h=' + repr(self._h)

        self._img = self._app_dialog.CaptureAsImage()
        if DEBUG_TRACE: print "taking screenshot" + self._timer.split_str()
        self._img_rgb = self._img.convert('RGB')
        if DEBUG_TRACE: print "convert to rgb" + self._timer.split_str()
        self._px = self._img_rgb.load()
        if DEBUG_TRACE: print "load pixels" + self._timer.split_str()
        #self._px = np.array(self._img_rgb)
        #if DEBUG_TRACE: print "load pixels to numpy array"
        
        self._pos_top_left = self._find_board_top_left_corner()
        if DEBUG_TRACE: print "find top left corner" + self._timer.split_str()
        if self._pos_top_left == False: return False
        
        self._pos_start_button = self._find_start_game_button()
        if DEBUG_TRACE: print "find start button" + self._timer.split_str()
        if self._pos_start_button == False: return False
        
        # check if game is already won or lost
        pos = (self._pos_start_button[X]+self._SMILEY_GAME_WON_IF_BLACK_OFFSET[X],
               self._pos_start_button[Y]+self._SMILEY_GAME_WON_IF_BLACK_OFFSET[Y])
        if not self._getpixel(pos) == self._COLOR_START_BUTTON_SMILEY_BG:
            self.state = WON
            return False
        pos = (self._pos_start_button[X]+self._SMILEY_GAME_LOST_IF_BLACK_OFFSET[X],
               self._pos_start_button[Y]+self._SMILEY_GAME_LOST_IF_BLACK_OFFSET[Y])
        if not self._getpixel(pos) == self._COLOR_START_BUTTON_SMILEY_BG:
            self.state = LOST
            return False
        
        self.state = IN_PROGRESS

        self.mine_count = self._get_mine_count()
        if DEBUG_TRACE: print "get mine count" + self._timer.split_str()
        
        self._board_dims = self._find_board_dimensions()
        if DEBUG_TRACE: print "get board dims" + self._timer.split_str()
        self._board = self._parse_board()
        if DEBUG_TRACE: print "parse board" + self._timer.split_str()
        if DEBUG_TRACE: print "capture done" + self._timer.split_str()
        return True
    
    # assumes components R, G, B
    def _getpixel(self, (x, y)):
        return self._px[x,y][0], self._px[x,y][1], self._px[x,y][2]
        #return self._px[y, x, 0], self._px[y, x, 1], self._px[y, x, 2]
    
    def _refresh_bounds(self):
        self._bounds = self._robot.get_window_bounds()
        self._x = self._bounds[LEFT]
        self._y = self._bounds[TOP]
        self._w = self._bounds[RIGHT] - self._bounds[LEFT]
        self._h = self._bounds[BOTTOM] - self._bounds[TOP]

    def _parse_board(self):
        return [[self._get_cell_value(x, y)
                    for x in range(self._board_dims[W])]
                    for y in range(self._board_dims[H])]

    def _find_board_top_left_corner(self):
        #if VERBOSE: print 'Locating top left corner...',
        for y in range(self._h):
            for x in range(min(100, self._w)):
                if self._getpixel((x, y)) == self._COLOR_WHITE_TL_CORNER:
                    good = True
                    for k in range(50):
                        if x+k > self._w or self._getpixel((x+k, y)) != self._COLOR_WHITE_TL_CORNER:
                            good = False
                    if good: 
                        #if VERBOSE: print 'found it at ' + repr((x, y))
                        return x, y
        if VERBOSE: print 'ERROR: could not find white top left corner of board'
        return False

    def _find_start_game_button(self):
        #if VERBOSE: print 'Locating start game button...',
        for y in range(self._h):
            for x in range(self._w):
                #if self._getpixel((x, y)) == self._COLOR_START_BUTTON_SMILEY_BG:
                if (self._px[x,y][0] == self._COLOR_START_BUTTON_SMILEY_BG[0] and
                        self._px[x,y][1] == self._COLOR_START_BUTTON_SMILEY_BG[1] and
                        self._px[x,y][2] == self._COLOR_START_BUTTON_SMILEY_BG[2]):
                #if (self._px[y,x,0] == self._COLOR_START_BUTTON_SMILEY_BG[0] and
                #        self._px[y,x,1] == self._COLOR_START_BUTTON_SMILEY_BG[1] and
                #        self._px[y,x,2] == self._COLOR_START_BUTTON_SMILEY_BG[2]):
                    #if VERBOSE: print 'found it at ' + repr((x, y))
                    return x, y
                
        if VERBOSE: print 'ERROR: could not find start game button with background color ' + repr(self._COLOR_START_BUTTON_SMILEY_BG)
        return False

    def _get_cell_pos(self, cellx, celly):
        x = self._pos_top_left[X] + self._BOARD_OFFSET_FROM_TL_CORNER[X] + cellx*self._CELL_SIZE
        y = self._pos_top_left[Y] + self._BOARD_OFFSET_FROM_TL_CORNER[Y] + celly*self._CELL_SIZE
        #print 'cell (' + repr(cellx) + ', ' + repr(celly) + ') has pixel location (' + repr(x) + ', ' + repr(y) + ')'
        return x, y
    
#    def _get_cell_px(self, cellx, celly, offsetx, offsety):
#        pos = self._get_cell_pos(cellx, celly)
#        return self._getpixel((pos[X] + offsetx, pos[Y] + offsety))
    
    def _get_cell_center(self, cellx, celly):
        pos = self._get_cell_pos(cellx, celly)
        return (pos[X] + self._CELL_CENTER_OFFSET[X], pos[Y] + self._CELL_CENTER_OFFSET[Y])
    
    def _get_cell_value(self, cellx, celly):
        return cell_imghashes[self._get_cell_imagehash(cellx, celly)]
    
    def _get_cell_imagehash(self, cellx, celly):
        pos = self._get_cell_pos(cellx, celly)
        return self._get_imagehash(
                (pos[X], pos[Y], pos[X]+self._CELL_SIZE, pos[Y]+self._CELL_SIZE),
                cell_imghashes,
                unknown_imghashes)
#        imagehash = 0
#        for y in range(pos[Y], pos[Y]+self._CELL_SIZE):
#            for x in range(pos[X], pos[X]+self._CELL_SIZE):
#                color = self._getpixel((x, y))
#                imagehash = imagehash + color[R]*256*256 + color[G]*256 + color[B]
#        if cell_imghashes.get(imagehash) == None:
#            print 'ERROR: unknown imagehash=' + repr(imagehash) + ' for cell ' + repr((cellx, celly))
#            self._img.show()
#            exit()
#        return imagehash

    def _find_board_dimensions(self):
        pos = self._get_cell_pos(0, 0)
        rows = []
        for celly in range((self._h - pos[Y]) // self._CELL_SIZE):
            if cell_imghashes[self._get_cell_imagehash(0, celly)] == NOT_A_CELL: break
            else: rows.append(celly)
        cols = []
        for cellx in range((self._w - pos[X]) // self._CELL_SIZE):
            if cell_imghashes[self._get_cell_imagehash(cellx, 0)] == NOT_A_CELL: break
            else: cols.append(cellx)
        return len(cols), len(rows)
    
    def _handle_unknown_imagehash(self, known_imghashes, unknown_imghashes, imagehash, img):
        if known_imghashes.get(imagehash) == None:
            unknown_imghashes[imagehash] = img

    def _compute_imagehash(self, px, rect):
        #imagehash = np.uint64(0)
        imagehash = 0
        w = rect[RIGHT] - rect[LEFT]
        h = rect[BOTTOM] - rect[TOP]
        for offy in range(h):
            y = offy + rect[TOP]
            for offx in range(w):
                x = offx + rect[LEFT]
                c = px[x,y][0]*256*256 + px[x,y][1]*256 + px[x,y][2]
                #c = px[y, x, 0]*256*256 + px[y, x, 1]*256 + px[y, x, 2]
                imagehash = imagehash + c*(offy*w+offx)*(offy*w+offx)
                #imagehash = np.uint64(imagehash) + np.uint64(c)*np.uint64(y)*np.uint64(w) + np.uint64(c)*np.uint64(x)
        
            #imagehash = imagehash ^ (imagehash << 13)
            #imagehash = imagehash ^ (imagehash >> 17)
            #imagehash = imagehash ^ (imagehash >> 5)

            # randomize with xorshift
            #imagehash = np.uint64(imagehash) ^ (np.uint64(imagehash) << np.uint64(13))
            #imagehash = np.uint64(imagehash) ^ (np.uint64(imagehash) >> np.uint64(17))
            #imagehash = np.uint64(imagehash) ^ (np.uint64(imagehash) << np.uint64(5))

        # randomize with xorshift
        #imagehash = np.uint64(imagehash) ^ (np.uint64(imagehash) << np.uint64(13))
        #imagehash = np.uint64(imagehash) ^ (np.uint64(imagehash) >> np.uint64(17))
        #imagehash = np.uint64(imagehash) ^ (np.uint64(imagehash) << np.uint64(5))
        return imagehash

    def _get_imagehash(self, rect, known_imghashes, unknown_imghashes):
        imagehash = self._compute_imagehash(self._px, rect)
        self._handle_unknown_imagehash(known_imghashes, unknown_imghashes, imagehash, self._img.crop(rect))
        return imagehash

#    def _OLD_get_imagehash(self, rect, known_imghashes, unknown_imghashes):
#        imagehash = 0
#        for y in range(rect[TOP], rect[BOTTOM]):
#            for x in range(rect[LEFT], rect[RIGHT]):
#                color = self._img_rgb.getpixel((x, y))
#                c = color[R]*256*256 + color[G]*256 + color[B]
#                imagehash = np.uint64(imagehash) + np.uint64(c)*(np.uint64(y)-np.uint64(rect[TOP]))*(np.uint64(rect[RIGHT])-np.uint64(rect[LEFT])) + np.uint64(c)*(np.uint64(x)-np.uint64(rect[LEFT]))
#                imagehash = np.uint64(imagehash) ^ (np.uint64(imagehash) << np.uint64(13))
#                imagehash = np.uint64(imagehash) ^ (np.uint64(imagehash) >> np.uint64(17))
#                imagehash = np.uint64(imagehash) ^ (np.uint64(imagehash) << np.uint64(5))
#        if known_imghashes.get(imagehash) == None:
#            unknown_imghashes[imagehash] = self._img.crop(rect)
#            return False
#        return imagehash

    def _get_mine_count_digit_imagehash(self, digit_ix):
        assert digit_ix >= 0 and digit_ix <= self._MINE_COUNT_NUM_DIGITS
        posx = (self._pos_top_left[X] + self._MINE_COUNT_OFFSET_FROM_TL_CORNER[X]
                + digit_ix * self._MINE_COUNT_DIGIT_DIMS[W])
        posy = (self._pos_top_left[Y] + self._MINE_COUNT_OFFSET_FROM_TL_CORNER[Y])
        return self._get_imagehash(
                (posx, posy, posx+self._MINE_COUNT_DIGIT_DIMS[W], posy+self._MINE_COUNT_DIGIT_DIMS[H]),
                mine_count_digit_imghashes,
                unknown_imghashes)

    def _get_mine_count(self):
        if VERBOSE: print 'Fetching mine count...',
        result = ''
#        good = True
        for digit_ix in range(self._MINE_COUNT_NUM_DIGITS):
            digit_imagehash = self._get_mine_count_digit_imagehash(digit_ix)
#            if digit_imagehash == False: good = False
#            else:
#                digit = mine_count_digit_imghashes[digit_imagehash]
#                result = result + str(digit)
            digit = mine_count_digit_imghashes[digit_imagehash]
            result = result + str(digit)
#        if not good: return False
        if VERBOSE: print repr(result)
        return int(result)

def handle_exception():
    if len(unknown_imghashes) > 0:
        total_height = sum([value.size[H] for value in unknown_imghashes.itervalues()])
        total_width = max([value.size[W] for value in unknown_imghashes.itervalues()])
        img = Image.new('RGB', (total_width, total_height))
        offsety = 0
        pasted = 0
        print 'Unknown color sums:'
        for key, value in unknown_imghashes.iteritems():
            result = imagedict.lookup_image(value)
            if result == None:
                value.save('images/' + repr(key) + '.png', 'PNG')
                img.paste(value, (0, offsety))
                offsety = offsety + value.size[H]
                pasted = pasted + 1
                print repr(key)
            else:
                fname, img = result
                print repr(key) + ': ' + fname
            
        if pasted > 0: img.show()
