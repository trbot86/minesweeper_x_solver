from common import *
from PIL import Image
import os, os.path

class ImageDict:
    def __init__(self, path):
        self.images = dict()
        valid_images = [".jpg",".gif",".png",".tga"]
        for f in os.listdir(path):
            ext = os.path.splitext(f)[1]
            if ext.lower() not in valid_images: continue
            self.images[f] = Image.open(os.path.join(path,f))
        if VERBOSE: print 'Image dict contents:'
        for key, value in self.images.iteritems():
            print key
            #value.show()
    
    def lookup_image(self, img):
        w, h = img.size
        img_rgb = img.convert('RGB')
        px = img_rgb.load()
        
        for img2_fname, img2 in self.images.iteritems():
            w2, h2 = img2.size
            #print
            #print 'comparing img w,h=' + repr(w)+','+repr(h)+' with img2='+img2_fname+' w,h='+repr(w2)+','+repr(h2)
            if w2 != w or h2 != h: continue
            #print 'w,h are equal'
            img2_rgb = img2.convert('RGB')
            px2 = img2_rgb.load()
            
            good = True
            for y in range(h):
                for x in range(w):
                    if not px[x,y][R] == px2[x,y][R]:
                        #print 'px['+repr(x)+','+repr(y)+'][R]='+repr(px[x,y][R]) +' is not equal to px2['+repr(x)+','+repr(y)+'][R]='+repr(px2[x,y][R])
                        good = False
                    if not px[x,y][G] == px2[x,y][G]:
                        #print 'px['+repr(x)+','+repr(y)+'][G]='+repr(px[x,y][G]) +' is not equal to px2['+repr(x)+','+repr(y)+'][G]='+repr(px2[x,y][G])
                        good = False
                    if not px[x,y][B] == px2[x,y][B]:
                        #print 'px['+repr(x)+','+repr(y)+'][B]='+repr(px[x,y][B]) +' is not equal to px2['+repr(x)+','+repr(y)+'][B]='+repr(px2[x,y][B])
                        good = False
                    if not good: break
                if not good: break
            if good: return img2_fname, img2_rgb
        return None

#    def lookup_image(self, pixelmap, rect):
#        w = rect[RIGHT] - rect[LEFT] ## warning: does not handle differing width/heights between pixelmap and dict images
#        h = rect[BOTTOM] - rect[TOP]
#        for key, value in self.images.iteritems():
#            img_rgb = value.convert('RGB')
#            img_px = img_rgb.load()
#            good = True
#            for offy in range(h):
#                y = rect[TOP] + offy
#                for offx in range(w):
#                    x = rect[LEFT] + offx
#                    if pixelmap[x,y][R] != img_px[offx,offy][R]:
#                        good = False
#                        break
#                    if pixelmap[x,y][G] != img_px[offx,offy][G]:
#                        good = False
#                        break
#                    if pixelmap[x,y][B] != img_px[offx,offy][B]:
#                        good = False
#                        break
#                if not good: break
#            if good: return key, value
#        return None
